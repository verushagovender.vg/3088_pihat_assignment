# 3088_PiHat_Assignment
The Motor Driver feedback HAT is designed for a Raspberry Pi board that adds the ability of driving and controlling two DC motors and its speed.  It allows for controlling the direction of the motors such as reverse and forward. Furthermore, it also allows for tracking the position of its motors by using a Hall-effect Sensor.
